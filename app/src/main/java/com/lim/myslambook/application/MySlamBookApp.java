package com.lim.myslambook.application;

import android.app.Application;
import android.os.StrictMode;
import android.support.multidex.MultiDex;

import com.crashlytics.android.Crashlytics;
import com.lim.myslambook.R;
import com.lim.myslambook.storage.DatabaseUtil;
import com.lim.myslambook.storage.SharedPreferenceUtil;

import io.fabric.sdk.android.Fabric;

public class MySlamBookApp extends Application {
	@Override
	public void onCreate() {
		super.onCreate();
		MultiDex.install(this);
		Fabric.with(this, new Crashlytics());
		SharedPreferenceUtil.init(this);
		DatabaseUtil.init(this, getString(R.string.db_name), 1, null);
		StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
		StrictMode.setVmPolicy(builder.build());
	}

}