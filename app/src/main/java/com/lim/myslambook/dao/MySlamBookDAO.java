package com.lim.myslambook.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.lim.myslambook.storage.DatabaseUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class MySlamBookDAO {

    private static final String GET_SLAM_LIST = "SELECT slamId, realName FROM MySlamBook";
    private static final String GET_SLAMBOOK_DETIAL_BY_SLAM_ID = "SELECT * FROM MySlamBook WHERE slamId = ?";
    private static final String GET_SLAM_ID_FOR_DELETE = "SELECT * FROM DeleteSlam";

    public static void inserSlamDetails(ContentValues cv) {

        SQLiteDatabase db = null;
        try {

            db = DatabaseUtil.getDatabaseInstance();
            db.insert("MySlamBook", null, cv);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DatabaseUtil.closeResource(db, null, null);
        }
    }

    public static ArrayList<HashMap<String, String>> getAllSlam() {
        SQLiteDatabase db = null;
        Cursor c = null;
        ArrayList<HashMap<String, String>> slamList = new ArrayList<HashMap<String, String>>();
        try {

            db = DatabaseUtil.getDatabaseInstance();
            c = db.rawQuery(GET_SLAM_LIST, null);

            while (c.moveToNext()) {
                HashMap<String, String> data = new HashMap<String, String>();
                data.put("slamId", c.getString(c.getColumnIndex("slamId")));
                data.put("realName", c.getString(c.getColumnIndex("realName")));
                slamList.add(data);
            }

            Collections.reverse(slamList);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DatabaseUtil.closeResource(db, null, c);
        }

        return slamList;
    }

    public static HashMap<String, String> getSlamBookDetailsBySlamId(
            String slamId) {
        SQLiteDatabase db = null;
        Cursor c = null;
        HashMap<String, String> data = new HashMap<String, String>();
        try {

            db = DatabaseUtil.getDatabaseInstance();
            c = db.rawQuery(GET_SLAMBOOK_DETIAL_BY_SLAM_ID,
                    new String[]{slamId});

            while (c.moveToNext()) {
                data.put("slamId", c.getString(c.getColumnIndex("slamId")));
                data.put("realName", c.getString(c.getColumnIndex("realName")));
                data.put("nickName", c.getString(c.getColumnIndex("nickName")));
                data.put("birthdate",
                        c.getString(c.getColumnIndex("birthdate")));
                data.put("emailId", c.getString(c.getColumnIndex("emailId")));
                data.put("bestFriend",
                        c.getString(c.getColumnIndex("bestFriend")));
                data.put("happiestMomentOfLife",
                        c.getString(c.getColumnIndex("happiestMomentOfLife")));
                data.put("embarrassingMomentOfLife", c.getString(c
                        .getColumnIndex("embarrassingMomentOfLife")));
                data.put("threePlaceToSee",
                        c.getString(c.getColumnIndex("threePlaceToSee")));
                data.put("favColor", c.getString(c.getColumnIndex("favColor")));
                data.put("favSong", c.getString(c.getColumnIndex("favSong")));
                data.put("favMovie", c.getString(c.getColumnIndex("favMovie")));
                data.put("luckyNo", c.getString(c.getColumnIndex("luckyNo")));
                data.put("goodThing",
                        c.getString(c.getColumnIndex("goodThing")));
                data.put("badThing", c.getString(c.getColumnIndex("badThing")));
                data.put("funniestThingHappned",
                        c.getString(c.getColumnIndex("funniestThingHappned")));
                data.put("threeWish",
                        c.getString(c.getColumnIndex("threeWish")));
                data.put("ifYouCanInvisible",
                        c.getString(c.getColumnIndex("ifYouCanInvisible")));
                data.put("ifYouGetSuperPower",
                        c.getString(c.getColumnIndex("ifYouGetSuperPower")));
                data.put("biggestWhatIf",
                        c.getString(c.getColumnIndex("biggestWhatIf")));
                data.put("lifeIsHellWithout",
                        c.getString(c.getColumnIndex("lifeIsHellWithout")));
                data.put("wakingUpMonday",
                        c.getString(c.getColumnIndex("wakingUpMonday")));
                data.put("worstMistake",
                        c.getString(c.getColumnIndex("worstMistake")));
                data.put("commentAboutMe",
                        c.getString(c.getColumnIndex("commentAboutMe")));
                data.put("favQuote", c.getString(c.getColumnIndex("favQuote")));
                data.put("photo", c.getString(c.getColumnIndex("photo")));

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DatabaseUtil.closeResource(db, null, c);
        }

        return data;
    }

    public static void deleteSlamById(final Context mCotnext, final String slamId) {
        try {
            final SQLiteDatabase db = DatabaseUtil.getDatabaseInstance();
            Cursor c = null;

            // delete slambook record from local database of app
            db.delete("MySlamBook", "slamId = ?", new String[]{ slamId });
            DatabaseUtil.closeResource(db, null, c);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<String> getAllSlamIdForDelete() {
        SQLiteDatabase db = null;
        Cursor c = null;
        ArrayList<String> slamIdList = new ArrayList<String>();
        try {

            db = DatabaseUtil.getDatabaseInstance();
            c = db.rawQuery(GET_SLAM_ID_FOR_DELETE, null);

            while (c.moveToNext()) {
                slamIdList.add(c.getString(c.getColumnIndex("slamId")));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DatabaseUtil.closeResource(db, null, c);
        }

        return slamIdList;
    }

    public static void deleteSlamIdFromDeleteSlam(String slamId) {
        SQLiteDatabase db = null;

        try {
            db = DatabaseUtil.getDatabaseInstance();
            db.delete("DeleteSlam", "slamId = ?", new String[]{slamId});
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DatabaseUtil.closeResource(db, null, null);
        }
    }

    public static boolean checkIfSlamExistById(String slamId) {
        SQLiteDatabase db = null;
        Cursor c = null;
        boolean ans = false;
        try {

            db = DatabaseUtil.getDatabaseInstance();
            c = db.rawQuery(GET_SLAMBOOK_DETIAL_BY_SLAM_ID,
                    new String[]{slamId});

            ans = c.getCount() > 0;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DatabaseUtil.closeResource(db, null, c);
        }

        return ans;
    }

}
