package com.lim.myslambook.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.lim.myslambook.R;
import com.lim.myslambook.storage.SharedPreferenceUtil;

import java.io.ByteArrayOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;


public abstract class BaseActivity extends AppCompatActivity implements OnTouchListener {
	private Context mContext;
	protected ProgressDialog pDialog = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(getLayoutId());

		mContext = this;
		try {
			PackageInfo info = getPackageManager().getPackageInfo(
					"com.lim.myslambook", PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
//				Log.d("KeyHash:",
//						Base64.encodeToString(md.digest(), Base64.DEFAULT));
			}
		} catch (PackageManager.NameNotFoundException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}

	protected void setTypeface(View view, String fontType) {
		Typeface typeface = Typeface.createFromAsset(getAssets(), fontType);
		if (view instanceof EditText) {
			((EditText) view).setTypeface(typeface);
		} else if (view instanceof TextView) {
			((TextView) view).setTypeface(typeface);
		} else if (view instanceof Button) {
			((Button) view).setTypeface(typeface);
		}
	}

	public abstract int getLayoutId();

	@Override
	public boolean onTouch(View view, MotionEvent motionEvent) {
		// TODO Auto-generated method stub
		if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
			view.setAlpha(0.7f);
		} else {
			view.setAlpha(1f);
		}

		return false;
	}

	protected static boolean isValidEmail(CharSequence target) {
		if (target == null) {
			return false;
		} else {
			return android.util.Patterns.EMAIL_ADDRESS.matcher(target)
					.matches();
		}
	}

	protected void showToast(String message) {
		Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG)
				.show();
	}

	public void showProgressBar() {
		hideProgressBar();

		pDialog = new ProgressDialog(mContext);
		pDialog.setMessage(getString(R.string.loading));
		pDialog.show();
	}

	public void hideProgressBar() {
		if (pDialog != null && pDialog.isShowing()) {
			pDialog.dismiss();
		}
	}

	public String getUniqueId() {
		UUID uniqueKey = UUID.randomUUID();
		return uniqueKey.toString().replace("-", "") + "_"
				+ SharedPreferenceUtil.getString("userId", "");
	}

	protected String convertBitmapToBase64(Bitmap bitmap){
		String strImageEncode = "";
		try{
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.JPEG, 30, stream);
			byte[] image = stream.toByteArray();
			strImageEncode = Base64.encodeToString(image,
					Base64.DEFAULT);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return strImageEncode;
	}

	protected  Bitmap convertBase64ToBitmap(String string){
		try{
			byte [] encodeByte=Base64.decode(string,Base64.DEFAULT);
			return BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
		}
		catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

	protected void hideKeyboard() {
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		if(getCurrentFocus() != null)
			imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
	}
}
