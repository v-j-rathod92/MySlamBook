package com.lim.myslambook.slam;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.lim.myslambook.R;
import com.lim.myslambook.base.BaseActivity;
import com.lim.myslambook.utils.AdHandler;

import hotchemi.android.rate.AppRate;
import hotchemi.android.rate.OnClickButtonListener;

public class DashBoardActivity extends BaseActivity implements OnClickListener {

    private ImageButton imgBtnAddSlam, imgBtnViewSlam;
    private TextView txtTitle;
    private Context mContext;
    private AdView adView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        mContext = this;

        init();
        setFontTypeFace();
        setLisener();
        launchRateDialog();
    }

    private void init() {
        txtTitle = (TextView) findViewById(R.id.txtTitle);

        imgBtnAddSlam = (ImageButton) findViewById(R.id.imgBtnAddSlam);
        imgBtnViewSlam = (ImageButton) findViewById(R.id.imgBtnViewSlam);
        adView = (AdView) findViewById(R.id.adView);

        AdHandler adHandler = AdHandler.getInstance();
        AdRequest adRequest = adHandler.loadAd();

        AdListener adListener = new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                Log.e("load", "ok");
                adView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                Log.e("fail to load", "" + i);
            }
        };

        adView.setAdListener(adListener);
        adView.loadAd(adRequest);

        adHandler.loadFullScreenAd(this);
    }

    private void setFontTypeFace() {
        setTypeface(txtTitle, getString(R.string.font_handwritting_bold));
    }

    private void setLisener() {
        imgBtnAddSlam.setOnClickListener(this);
        imgBtnViewSlam.setOnClickListener(this);

        imgBtnAddSlam.setOnTouchListener(this);
        imgBtnViewSlam.setOnTouchListener(this);
    }

    @Override
    public int getLayoutId() {
        return R.layout.layout_dashboard;
    }

    @Override
    public void onClick(View view) {
        // TODO Auto-generated method stub
        Intent intent;
        switch (view.getId()) {
            case R.id.imgBtnAddSlam:
                intent = new Intent(mContext, AddSlamActivity.class);
                startActivity(intent);
                break;

            case R.id.imgBtnViewSlam:
                intent = new Intent(mContext, ViewSlamListActivity.class);
                startActivity(intent);
                break;
        }
    }

    private void launchRateDialog() {
        AppRate.with(this)
                .setInstallDays(0) // after 0 days of install, default 10
                .setLaunchTimes(10) // after 3 time launch, default 1
                .setRemindInterval(2) // after 2 days if user select remind me later, default 1
                .setShowLaterButton(true) // default true
                .setDebug(false) // default false
                .setOnClickButtonListener(new OnClickButtonListener() { // callback listener.
                    @Override
                    public void onClickButton(int which) {

                    }
                })
                .monitor();

        // Show a dialog if meets conditions
        AppRate.showRateDialogIfMeetsConditions(this);
    }
}
