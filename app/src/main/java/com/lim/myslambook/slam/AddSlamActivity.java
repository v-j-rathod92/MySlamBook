package com.lim.myslambook.slam;

import android.app.AlertDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.lim.myslambook.R;
import com.lim.myslambook.base.BaseActivity;
import com.lim.myslambook.dao.MySlamBookDAO;
import com.lim.myslambook.permission.GGPermissionManager;
import com.lim.myslambook.permission.OnRequestPermissionsCallBack;
import com.lim.myslambook.permission.PermissionRequest;
import com.lim.myslambook.picker.DatePickerFragment;
import com.lim.myslambook.storage.SharedPreferenceUtil;
import com.lim.myslambook.utils.AdHandler;
import com.lim.myslambook.utils.InternalStorageContentProvider;
import com.lim.myslambook.utils.PathUtil;
import com.makeramen.roundedimageview.RoundedImageView;
import com.soundcloud.android.crop.Crop;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

public class AddSlamActivity extends BaseActivity implements OnClickListener {
    private ImageButton imgBtnSave, imgBtnClear, imgBtnBack;
    private TextView txtTitle, txtRealName, txtFrndCallMe, txtBirthDate,
            txtEmail, txtBestFrnd, txtHappiestMoment, txtEmbarrassingMoment,
            txtThreePlace, txtFavColor, txtFavSong, txtFavMovie, txtLuckyNo,
            txtGoodThing, txtBadThing, txtFunniestMoment, txtThreeWish,
            txtIfYouCanInvisible, txtIfYouGetSuperpower, txtBiggestWhatIf,
            txtLifeIsHellWithout, txtWakingOnMonday, txtWorstMistake,
            txtCommentAboutMe, txtFavQuote;

    private EditText edtTxtRealName, edtTxtFrndCallMe, edtTxtBirthdate,
            edtTxtEmail, edtTxtBestFrnd, edtHappestMoment,
            edtTxtEmbarrassingMoment, edtTxtThreePlace, edtFavColor,
            edtTxtFavSong, edtTxtFavMovie, edtTxtLuckyNo, edtTxtGoogThing,
            edtTxtBadThing, edtTxtFunniestMoment, edtTxtThreeWish,
            edtTxtIfYouCanInvisible, edtTxtIfYouGetSuperpower,
            edtTxtBiggestWhatIf, edtTxtLifeIsHellWithout, edtTxtWakingOnMonday,
            edtTxtWorstMistake, edtTxtCommentAboutMe, edtTxtFavQuote;

    private String slamId, realName, nickName, birthdate, emailId, bestFriend,
            happiestMomentOfLife, embarrassingMomentOfLife, threePlaceToSee,
            favColor, favSong, favMovie, luckyNo, goodThing, badThing,
            funniestThingHappned, threeWish, ifYouCanInvisible,
            ifYouGetSuperPower, biggestWhatIf, lifeIsHellWithout,
            wakingUpMonday, worstMistake, commentAboutMe, favQuote,
            strBirthDate;
    private boolean isPicUploaded = false;
    private RoundedImageView imgDp;
    private File mFileTemp;
    private String state;
    private Bitmap uploadedPic;
    public static final int REQUEST_CODE_GALLERY = 0x1;
    public static final int REQUEST_CODE_TAKE_PICTURE = 0x2;

    private AdHandler adHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        init();
        setFontTypeFace();
        setLisener();
    }

    private void init() {

        txtTitle = (TextView) findViewById(R.id.txtTitle);

        txtRealName = (TextView) findViewById(R.id.txtRealName);
        txtFrndCallMe = (TextView) findViewById(R.id.txtFrndCallMe);
        txtBirthDate = (TextView) findViewById(R.id.txtBirthDate);
        txtEmail = (TextView) findViewById(R.id.txtEmail);
        txtBestFrnd = (TextView) findViewById(R.id.txtBestFrnd);
        txtHappiestMoment = (TextView) findViewById(R.id.txtHappiestMoment);
        txtEmbarrassingMoment = (TextView) findViewById(R.id.txtEmbarrassingMoment);
        txtThreePlace = (TextView) findViewById(R.id.txtThreePlace);
        txtFavColor = (TextView) findViewById(R.id.txtFavColor);
        txtFavSong = (TextView) findViewById(R.id.txtFavSong);
        txtFavMovie = (TextView) findViewById(R.id.txtFavMovie);
        txtLuckyNo = (TextView) findViewById(R.id.txtLuckyNo);
        txtGoodThing = (TextView) findViewById(R.id.txtGoodThing);
        txtBadThing = (TextView) findViewById(R.id.txtBadThing);
        txtFunniestMoment = (TextView) findViewById(R.id.txtFunniestMoment);
        txtThreeWish = (TextView) findViewById(R.id.txtThreeWish);
        txtIfYouCanInvisible = (TextView) findViewById(R.id.txtIfYouCanInvisible);
        txtIfYouGetSuperpower = (TextView) findViewById(R.id.txtIfYouGetSuperpower);
        txtBiggestWhatIf = (TextView) findViewById(R.id.txtBiggestWhatIf);
        txtLifeIsHellWithout = (TextView) findViewById(R.id.txtLifeIsHellWithout);
        txtWakingOnMonday = (TextView) findViewById(R.id.txtWakingOnMonday);
        txtWorstMistake = (TextView) findViewById(R.id.txtWorstMistake);
        txtCommentAboutMe = (TextView) findViewById(R.id.txtCommentAboutMe);
        txtFavQuote = (TextView) findViewById(R.id.txtFavQuote);

        edtTxtRealName = (EditText) findViewById(R.id.edtTxtRealName);
        edtTxtFrndCallMe = (EditText) findViewById(R.id.edtTxtFrndCallMe);
        edtTxtBirthdate = (EditText) findViewById(R.id.edtTxtBirthdate);
        edtTxtEmail = (EditText) findViewById(R.id.edtTxtEmail);
        edtTxtBestFrnd = (EditText) findViewById(R.id.edtTxtBestFrnd);
        edtHappestMoment = (EditText) findViewById(R.id.edtHappestMoment);
        edtTxtEmbarrassingMoment = (EditText) findViewById(R.id.edtTxtEmbarrassingMoment);
        edtTxtThreePlace = (EditText) findViewById(R.id.edtTxtThreePlace);
        edtFavColor = (EditText) findViewById(R.id.edtFavColor);
        edtTxtFavSong = (EditText) findViewById(R.id.edtTxtFavSong);
        edtTxtFavMovie = (EditText) findViewById(R.id.edtTxtFavMovie);
        edtTxtLuckyNo = (EditText) findViewById(R.id.edtTxtLuckyNo);
        edtTxtGoogThing = (EditText) findViewById(R.id.edtTxtGoogThing);
        edtTxtBadThing = (EditText) findViewById(R.id.edtTxtBadThing);
        edtTxtFunniestMoment = (EditText) findViewById(R.id.edtTxtFunniestMoment);
        edtTxtThreeWish = (EditText) findViewById(R.id.edtTxtThreeWish);
        edtTxtIfYouCanInvisible = (EditText) findViewById(R.id.edtTxtIfYouCanInvisible);
        edtTxtIfYouGetSuperpower = (EditText) findViewById(R.id.edtTxtIfYouGetSuperpower);
        edtTxtBiggestWhatIf = (EditText) findViewById(R.id.edtTxtBiggestWhatIf);
        edtTxtLifeIsHellWithout = (EditText) findViewById(R.id.edtTxtLifeIsHellWithout);
        edtTxtWakingOnMonday = (EditText) findViewById(R.id.edtTxtWakingOnMonday);
        edtTxtWorstMistake = (EditText) findViewById(R.id.edtTxtWorstMistake);
        edtTxtCommentAboutMe = (EditText) findViewById(R.id.edtTxtCommentAboutMe);
        edtTxtFavQuote = (EditText) findViewById(R.id.edtTxtFavQuote);

        state = Environment.getExternalStorageState();

        imgDp = (RoundedImageView) findViewById(R.id.imgDp);

        imgBtnSave = (ImageButton) findViewById(R.id.imgBtnSave);
        imgBtnClear = (ImageButton) findViewById(R.id.imgBtnClear);
        imgBtnBack = (ImageButton) findViewById(R.id.imgBtnBack);

        adHandler = AdHandler.getInstance();
    }

    private void setFontTypeFace() {
        setTypeface(txtTitle, getString(R.string.font_handwritting_bold));

        setTypeface(edtTxtRealName, getString(R.string.font_handwritting_bold));
        setTypeface(edtTxtFrndCallMe, getString(R.string.font_handwritting_bold));
        setTypeface(edtTxtBirthdate, getString(R.string.font_handwritting_bold));
        setTypeface(edtTxtEmail, getString(R.string.font_handwritting_bold));
        setTypeface(edtTxtBestFrnd, getString(R.string.font_handwritting_bold));
        setTypeface(edtHappestMoment, getString(R.string.font_handwritting_bold));
        setTypeface(edtTxtEmbarrassingMoment, getString(R.string.font_handwritting_bold));
        setTypeface(edtTxtThreePlace, getString(R.string.font_handwritting_bold));
        setTypeface(edtFavColor, getString(R.string.font_handwritting_bold));
        setTypeface(edtTxtFavSong, getString(R.string.font_handwritting_bold));
        setTypeface(edtTxtFavMovie, getString(R.string.font_handwritting_bold));
        setTypeface(edtTxtLuckyNo, getString(R.string.font_handwritting_bold));
        setTypeface(edtTxtGoogThing, getString(R.string.font_handwritting_bold));
        setTypeface(edtTxtBadThing, getString(R.string.font_handwritting_bold));
        setTypeface(edtTxtFunniestMoment, getString(R.string.font_handwritting_bold));
        setTypeface(edtTxtThreeWish, getString(R.string.font_handwritting_bold));
        setTypeface(edtTxtIfYouCanInvisible, getString(R.string.font_handwritting_bold));
        setTypeface(edtTxtIfYouGetSuperpower, getString(R.string.font_handwritting_bold));
        setTypeface(edtTxtBiggestWhatIf, getString(R.string.font_handwritting_bold));
        setTypeface(edtTxtLifeIsHellWithout, getString(R.string.font_handwritting_bold));
        setTypeface(edtTxtWakingOnMonday, getString(R.string.font_handwritting_bold));
        setTypeface(edtTxtWorstMistake, getString(R.string.font_handwritting_bold));
        setTypeface(edtTxtCommentAboutMe, getString(R.string.font_handwritting_bold));
        setTypeface(edtTxtFavQuote, getString(R.string.font_handwritting_bold));

        setTypeface(txtRealName, getString(R.string.font_handwritting_bold));
        setTypeface(txtFrndCallMe, getString(R.string.font_handwritting_bold));
        setTypeface(txtBirthDate, getString(R.string.font_handwritting_bold));
        setTypeface(txtEmail, getString(R.string.font_handwritting_bold));
        setTypeface(txtBestFrnd, getString(R.string.font_handwritting_bold));
        setTypeface(txtHappiestMoment,
                getString(R.string.font_handwritting_bold));
        setTypeface(txtEmbarrassingMoment,
                getString(R.string.font_handwritting_bold));
        setTypeface(txtThreePlace, getString(R.string.font_handwritting_bold));
        setTypeface(txtFavColor, getString(R.string.font_handwritting_bold));
        setTypeface(txtFavSong, getString(R.string.font_handwritting_bold));
        setTypeface(txtFavMovie, getString(R.string.font_handwritting_bold));
        setTypeface(txtLuckyNo, getString(R.string.font_handwritting_bold));
        setTypeface(txtGoodThing, getString(R.string.font_handwritting_bold));
        setTypeface(txtBadThing, getString(R.string.font_handwritting_bold));
        setTypeface(txtFunniestMoment,
                getString(R.string.font_handwritting_bold));
        setTypeface(txtThreeWish, getString(R.string.font_handwritting_bold));
        setTypeface(txtIfYouCanInvisible,
                getString(R.string.font_handwritting_bold));
        setTypeface(txtIfYouGetSuperpower,
                getString(R.string.font_handwritting_bold));
        setTypeface(txtBiggestWhatIf,
                getString(R.string.font_handwritting_bold));
        setTypeface(txtLifeIsHellWithout,
                getString(R.string.font_handwritting_bold));
        setTypeface(txtWakingOnMonday,
                getString(R.string.font_handwritting_bold));
        setTypeface(txtWorstMistake, getString(R.string.font_handwritting_bold));
        setTypeface(txtCommentAboutMe,
                getString(R.string.font_handwritting_bold));
        setTypeface(txtFavQuote, getString(R.string.font_handwritting_bold));

    }

    private void setLisener() {
        imgBtnSave.setOnClickListener(this);
        imgBtnClear.setOnClickListener(this);
        imgBtnBack.setOnClickListener(this);
        edtTxtBirthdate.setOnClickListener(this);
        imgDp.setOnClickListener(this);

        imgBtnSave.setOnTouchListener(this);
        imgBtnClear.setOnTouchListener(this);
        imgBtnBack.setOnTouchListener(this);

    }

    @Override
    public int getLayoutId() {
        // TODO Auto-generated method stub
        return R.layout.layout_add_slam;
    }

    @Override
    public void onClick(View view) {
        // TODO Auto-generated method stub
        switch (view.getId()) {
            case R.id.imgBtnSave:
                if (validate()) {
                    storeUserSlamDetails();
                    finish();
                }
                break;

            case R.id.imgBtnBack:
                onBackPressed();
                break;

            case R.id.imgBtnClear:
                clearData();
                break;

            case R.id.edtTxtBirthdate:
                showDatePicker();
                break;

            case R.id.imgDp:
                new GGPermissionManager.Builder(this)
                        .addPermissions(PermissionRequest.cameraAndGalleryPermission())
                        .addRequestPermissionsCallBack(new OnRequestPermissionsCallBack() {
                            @Override
                            public void onGrant() {
                                uploadPicture();
                            }

                            @Override
                            public void onDenied(String permission) {
                                Log.e("permission", "denied");
                            }

                        }).build().request();
                break;

        }

    }

    private boolean validate() {
        int count = 1;

        slamId = getUniqueId();
        realName = edtTxtRealName.getText().toString().trim();
        nickName = edtTxtFrndCallMe.getText().toString().trim();
        birthdate = edtTxtBirthdate.getText().toString().trim();
        emailId = edtTxtEmail.getText().toString().trim();
        bestFriend = edtTxtBestFrnd.getText().toString().trim();
        happiestMomentOfLife = edtHappestMoment.getText().toString().trim();
        embarrassingMomentOfLife = edtTxtEmbarrassingMoment.getText()
                .toString().trim();
        threePlaceToSee = edtTxtThreePlace.getText().toString().trim();
        favColor = edtFavColor.getText().toString().trim();
        favSong = edtTxtFavSong.getText().toString().trim();
        favMovie = edtTxtFavMovie.getText().toString().trim();
        luckyNo = edtTxtLuckyNo.getText().toString().trim();
        goodThing = edtTxtGoogThing.getText().toString().trim();
        badThing = edtTxtBadThing.getText().toString().trim();
        funniestThingHappned = edtTxtFunniestMoment.getText().toString().trim();
        threeWish = edtTxtThreeWish.getText().toString().trim();
        ifYouCanInvisible = edtTxtIfYouCanInvisible.getText().toString().trim();
        ifYouGetSuperPower = edtTxtIfYouGetSuperpower.getText().toString()
                .trim();
        biggestWhatIf = edtTxtBiggestWhatIf.getText().toString().trim();
        lifeIsHellWithout = edtTxtLifeIsHellWithout.getText().toString().trim();
        wakingUpMonday = edtTxtWakingOnMonday.getText().toString().trim();
        worstMistake = edtTxtWorstMistake.getText().toString().trim();
        commentAboutMe = edtTxtCommentAboutMe.getText().toString().trim();
        favQuote = edtTxtFavQuote.getText().toString().trim();

        if (realName.length() < 1) {
            showToast(getString(R.string.enter_real_name));
            return false;
        }

        if (emailId.trim().length() > 0) {
            if (!isValidEmail(edtTxtEmail.getText().toString())) {
                showToast(getString(R.string.enter_valid_email));
                return false;
            }
        }

        if (nickName.length() > 0) {
            count++;
        }

        if (birthdate.length() > 0) {
            count++;
        }

        if (emailId.length() > 0) {
            count++;
        }

        if (bestFriend.length() > 0) {
            count++;
        }

        if (happiestMomentOfLife.length() > 0) {
            count++;
        }

        if (embarrassingMomentOfLife.length() > 0) {
            count++;
        }

        if (threePlaceToSee.length() > 0) {
            count++;
        }

        if (favColor.length() > 0) {
            count++;
        }

        if (favSong.length() > 0) {
            count++;
        }

        if (favMovie.length() > 0) {
            count++;
        }

        if (luckyNo.length() > 0) {
            count++;
        }

        if (goodThing.length() > 0) {
            count++;
        }

        if (badThing.length() > 0) {
            count++;
        }

        if (funniestThingHappned.length() > 0) {
            count++;
        }

        if (threeWish.length() > 0) {
            count++;
        }

        if (ifYouCanInvisible.length() > 0) {
            count++;
        }

        if (ifYouGetSuperPower.length() > 0) {
            count++;
        }

        if (biggestWhatIf.length() > 0) {
            count++;
        }

        if (lifeIsHellWithout.length() > 0) {
            count++;
        }

        if (wakingUpMonday.length() > 0) {
            count++;
        }

        if (worstMistake.length() > 0) {
            count++;
        }

        if (commentAboutMe.length() > 0) {
            count++;
        }

        if (favQuote.length() > 0) {
            count++;
        }

        if (count <= 4) {
            showToast(getString(R.string.fill_minimum_five_detail));
            return false;
        } else {
            return true;
        }

    }

    private void storeUserSlamDetails() {
        ContentValues cv = new ContentValues();

        cv.put("slamId", slamId);
        cv.put("userId", SharedPreferenceUtil.getString("userId", ""));
        cv.put("realName", realName);
        cv.put("nickName", nickName);
        cv.put("birthdate", birthdate);
        cv.put("emailId", emailId);
        cv.put("bestFriend", bestFriend);
        cv.put("happiestMomentOfLife", happiestMomentOfLife);
        cv.put("embarrassingMomentOfLife", embarrassingMomentOfLife);
        cv.put("threePlaceToSee", threePlaceToSee);
        cv.put("favColor", favColor);
        cv.put("favSong", favSong);
        cv.put("favMovie", favMovie);
        cv.put("luckyNo", luckyNo);
        cv.put("goodThing", goodThing);
        cv.put("badThing", badThing);
        cv.put("funniestThingHappned", funniestThingHappned);
        cv.put("threeWish", threeWish);
        cv.put("ifYouCanInvisible", ifYouCanInvisible);
        cv.put("ifYouGetSuperPower", ifYouGetSuperPower);
        cv.put("biggestWhatIf", biggestWhatIf);
        cv.put("lifeIsHellWithout", lifeIsHellWithout);
        cv.put("wakingUpMonday", wakingUpMonday);
        cv.put("worstMistake", worstMistake);
        cv.put("commentAboutMe", commentAboutMe);
        cv.put("favQuote", favQuote);
        if (isPicUploaded) {
            cv.put("photo", convertBitmapToBase64(uploadedPic));
        } else {
            cv.put("photo", "");
        }
        MySlamBookDAO.inserSlamDetails(cv);


    }

    private void showDatePicker() {
        DatePickerFragment datePicker = new DatePickerFragment(
                AddSlamActivity.this);

        Calendar calender = Calendar.getInstance();

        Bundle args = new Bundle();
        args.putInt("year", calender.get(Calendar.YEAR));
        args.putInt("month", calender.get(Calendar.MONTH));
        args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));

        datePicker.setArguments(args);
        datePicker.setCallBack(onDateSet);

        datePicker.show(getSupportFragmentManager(), "Date Picker");

    }

    OnDateSetListener onDateSet = new OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int years, int monthOfYear,
                              int dayOfMonth) {

            strBirthDate = dayOfMonth + " - " + (monthOfYear + 1) + " - " + years;

            edtTxtBirthdate.setText(strBirthDate);
        }
    };

    private void uploadPicture() {
        String[] option2;

        if (isPicUploaded) {
            option2 = new String[]{"Remove Photo", "Camera", "Gallery"};
        } else {
            option2 = new String[]{"Camera", "Gallery"};
        }


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.select_dialog_item, option2);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Option");
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {

                if (isPicUploaded) {
                    if (which == 0) {
                        imgDp.setImageResource(R.mipmap.dp_frame);
                        isPicUploaded = false;
                    }
                    if (which == 1) {
                        takePicture();
                    }
                    if (which == 2) {
                        openGallery();
                    }
                } else {
                    if (which == 0) {
                        takePicture();
                    }
                    if (which == 1) {
                        openGallery();
                    }
                }
            }
        });
        final AlertDialog dialog = builder.create();
        dialog.show();
    }

    protected void takePicture() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            mFileTemp = new File(Environment.getExternalStorageDirectory(),
                    "temp_photo" + ".jpg");
        } else {
            mFileTemp = new File(this.getFilesDir(), "temp_photo" + ".jpg");
        }

        try {
            Uri mImageCaptureUri = null;
            String state = Environment.getExternalStorageState();
            if (Environment.MEDIA_MOUNTED.equals(state)) {
                mImageCaptureUri = Uri.fromFile(mFileTemp);
            } else {
                /*
                 * The solution is taken from here: http://stackoverflow.com/questions/10042695/how-to-get-camera-result-as-a-uri-in-data-folder
                 */
                mImageCaptureUri = InternalStorageContentProvider.CONTENT_URI;
            }
            intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
        } catch (ActivityNotFoundException e) {

        }
    }

    protected void openGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);
    }

    private void rotateCameraPicIfNeeded() {
        try {
            int rotate = 0;
            String photopath = mFileTemp.getPath();
            Bitmap bmp = BitmapFactory.decodeFile(photopath);

            ExifInterface exif = new ExifInterface(mFileTemp.getPath());

            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }


            Matrix matrix = new Matrix();
            matrix.postRotate(rotate);
            bmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);

            FileOutputStream fOut;
            fOut = new FileOutputStream(mFileTemp);
            bmp.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
            fOut.flush();
            fOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent result) {

        if ((requestCode == REQUEST_CODE_TAKE_PICTURE)
                && resultCode == RESULT_OK) {

            rotateCameraPicIfNeeded();
            beginCrop(Uri.fromFile(mFileTemp));

        } else if (requestCode == REQUEST_CODE_GALLERY && resultCode == RESULT_OK) {

            beginCrop(result.getData());
        } else if (requestCode == Crop.REQUEST_CROP) {

            handleCrop(resultCode, result);
        }
    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(this);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            try {
                uploadedPic = adjustImageOrientation(PathUtil.getPath(this, Crop.getOutput(result)));
                imgDp.setImageBitmap(uploadedPic);
                isPicUploaded = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void clearData() {
        edtTxtRealName.setText("");
        edtTxtFrndCallMe.setText("");
        edtTxtBirthdate.setText("");
        edtTxtEmail.setText("");
        edtTxtBestFrnd.setText("");
        edtHappestMoment.setText("");
        edtTxtEmbarrassingMoment.setText("");
        edtTxtThreePlace.setText("");
        edtFavColor.setText("");
        edtTxtFavSong.setText("");
        edtTxtFavMovie.setText("");
        edtTxtLuckyNo.setText("");
        edtTxtGoogThing.setText("");
        edtTxtBadThing.setText("");
        edtTxtFunniestMoment.setText("");
        edtTxtThreeWish.setText("");
        edtTxtIfYouCanInvisible.setText("");
        edtTxtIfYouGetSuperpower.setText("");
        edtTxtBiggestWhatIf.setText("");
        edtTxtLifeIsHellWithout.setText("");
        edtTxtWakingOnMonday.setText("");
        edtTxtWorstMistake.setText("");
        edtTxtCommentAboutMe.setText("");
        edtTxtFavQuote.setText("");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        adHandler.showFullScrenAd();
    }

    private Bitmap adjustImageOrientation(String imagePath) {
        ExifInterface exif;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inScaled = false;
        Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
        try {
            exif = new ExifInterface(imagePath);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
            } else if (orientation == 3) {
                matrix.postRotate(180);
            } else if (orientation == 8) {
                matrix.postRotate(270);
            }

            bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return bitmap;
    }
}
