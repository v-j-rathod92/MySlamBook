package com.lim.myslambook.slam;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.lim.myslambook.R;
import com.lim.myslambook.adapter.ViewSlamAdapter;
import com.lim.myslambook.base.BaseActivity;
import com.lim.myslambook.dao.MySlamBookDAO;
import com.lim.myslambook.utils.AdHandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

public class ViewSlamListActivity extends BaseActivity implements OnClickListener {

    private TextView txtTitle;
    private ImageButton imgBtnBack;
    private EditText edtTxtSearchSlam;
    private Context mContext;
    private ViewSlamAdapter adapter;
    private AdHandler adHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        mContext = this;

        init();
        setFontTypeFace();
        setLisener();
    }

    private void init() {
        txtTitle = (TextView) findViewById(R.id.txtTitle);
        imgBtnBack = (ImageButton) findViewById(R.id.imgBtnBack);
        edtTxtSearchSlam = (EditText) findViewById(R.id.edtTxtSearchSlam);

        ListView listViewSlam = (ListView) findViewById(R.id.listViewSlam);

        ArrayList<HashMap<String, String>> slamList = MySlamBookDAO.getAllSlam();
        adapter = new ViewSlamAdapter(slamList, mContext);
        listViewSlam.setAdapter(adapter);


        edtTxtSearchSlam.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String text = edtTxtSearchSlam.getText().toString().toLowerCase(Locale.getDefault());
                adapter.filter(text);
            }
        });

        adHandler = AdHandler.getInstance();
    }

    private void setFontTypeFace() {
        setTypeface(txtTitle, getString(R.string.font_handwritting_bold));
        setTypeface(edtTxtSearchSlam, getString(R.string.font_handwritting_bold));
    }

    private void setLisener() {
        imgBtnBack.setOnClickListener(this);
        imgBtnBack.setOnTouchListener(this);

    }

    @Override
    public void onClick(View view) {
        // TODO Auto-generated method stub
        switch (view.getId()) {
            case R.id.imgBtnBack:
                onBackPressed();
                break;
        }
    }

    @Override
    public int getLayoutId() {
        // TODO Auto-generated method stub
        return R.layout.layout_view_slam_list;

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        adHandler.showFullScrenAd();
    }
}
