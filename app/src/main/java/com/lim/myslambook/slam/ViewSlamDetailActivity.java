package com.lim.myslambook.slam;

import java.util.HashMap;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.lim.myslambook.R;
import com.lim.myslambook.base.BaseActivity;
import com.lim.myslambook.dao.MySlamBookDAO;
import com.makeramen.roundedimageview.RoundedImageView;

public class ViewSlamDetailActivity extends BaseActivity implements
		OnClickListener {
	private TextView txtTitle;
	private ImageButton imgBtnBack, imgBtnDelete;
	private TextView txt1, txt2, txt3, txt4, txt5, txt6, txt7, txt8, txt9,
			txt10, txt11, txt12, txt13, txt14, txt15, txt16, txt17, txt18,
			txt19, txt20, txt21, txt22, txt23, txt24;
	private TextView txtRealName, txtFrndCallMe, txtBirthDate, txtEmail,
			txtBestFrnd, txtHappiestMoment, txtEmbarrassingMoment,
			txtThreePlace, txtFavColor, txtFavSong, txtFavMovie, txtLuckyNo,
			txtGoodThing, txtBadThing, txtFunniestMoment, txtThreeWish,
			txtIfYouCanInvisible, txtIfYouGetSuperpower, txtBiggestWhatIf,
			txtLifeIsHellWithout, txtWakingOnMonday, txtWorstMistake,
			txtCommentAboutMe, txtFavQuote;
	private RoundedImageView imgDp;
	private Context mContext;
	private Intent intent;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		mContext = this;

		init();
		setFontTypeFace();
		setLisener();
		setSlamDetails();
	}

	private void init() {
		txtTitle = (TextView) findViewById(R.id.txtTitle);
		imgBtnBack = (ImageButton) findViewById(R.id.imgBtnBack);
		imgBtnDelete = (ImageButton) findViewById(R.id.imgBtnDelete);
		imgDp = (RoundedImageView) findViewById(R.id.imgDp);

		txt1 = (TextView) findViewById(R.id.txt1);
		txt2 = (TextView) findViewById(R.id.txt2);
		txt3 = (TextView) findViewById(R.id.txt3);
		txt4 = (TextView) findViewById(R.id.txt4);
		txt5 = (TextView) findViewById(R.id.txt5);
		txt6 = (TextView) findViewById(R.id.txt6);
		txt7 = (TextView) findViewById(R.id.txt7);
		txt8 = (TextView) findViewById(R.id.txt8);
		txt9 = (TextView) findViewById(R.id.txt9);
		txt10 = (TextView) findViewById(R.id.txt10);
		txt11 = (TextView) findViewById(R.id.txt11);
		txt12 = (TextView) findViewById(R.id.txt12);
		txt13 = (TextView) findViewById(R.id.txt13);
		txt14 = (TextView) findViewById(R.id.txt14);
		txt15 = (TextView) findViewById(R.id.txt15);
		txt16 = (TextView) findViewById(R.id.txt16);
		txt17 = (TextView) findViewById(R.id.txt17);
		txt18 = (TextView) findViewById(R.id.txt18);
		txt19 = (TextView) findViewById(R.id.txt19);
		txt20 = (TextView) findViewById(R.id.txt20);
		txt21 = (TextView) findViewById(R.id.txt21);
		txt22 = (TextView) findViewById(R.id.txt22);
		txt23 = (TextView) findViewById(R.id.txt23);
		txt24 = (TextView) findViewById(R.id.txt24);

		txtRealName = (TextView) findViewById(R.id.txtRealName);
		txtFrndCallMe = (TextView) findViewById(R.id.txtFrndCallMe);
		txtBirthDate = (TextView) findViewById(R.id.txtBirthDate);
		txtEmail = (TextView) findViewById(R.id.txtEmail);
		txtBestFrnd = (TextView) findViewById(R.id.txtBestFrnd);
		txtHappiestMoment = (TextView) findViewById(R.id.txtHappiestMoment);
		txtEmbarrassingMoment = (TextView) findViewById(R.id.txtEmbarrassingMoment);
		txtThreePlace = (TextView) findViewById(R.id.txtThreePlace);
		txtFavColor = (TextView) findViewById(R.id.txtFavColor);
		txtFavSong = (TextView) findViewById(R.id.txtFavSong);
		txtFavMovie = (TextView) findViewById(R.id.txtFavMovie);
		txtLuckyNo = (TextView) findViewById(R.id.txtLuckyNo);
		txtGoodThing = (TextView) findViewById(R.id.txtGoodThing);
		txtBadThing = (TextView) findViewById(R.id.txtBadThing);
		txtFunniestMoment = (TextView) findViewById(R.id.txtFunniestMoment);
		txtThreeWish = (TextView) findViewById(R.id.txtThreeWish);
		txtIfYouCanInvisible = (TextView) findViewById(R.id.txtIfYouCanInvisible);
		txtIfYouGetSuperpower = (TextView) findViewById(R.id.txtIfYouGetSuperpower);
		txtBiggestWhatIf = (TextView) findViewById(R.id.txtBiggestWhatIf);
		txtLifeIsHellWithout = (TextView) findViewById(R.id.txtLifeIsHellWithout);
		txtWakingOnMonday = (TextView) findViewById(R.id.txtWakingOnMonday);
		txtWorstMistake = (TextView) findViewById(R.id.txtWorstMistake);
		txtCommentAboutMe = (TextView) findViewById(R.id.txtCommentAboutMe);
		txtFavQuote = (TextView) findViewById(R.id.txtFavQuote);
	}

	private void setFontTypeFace() {
		setTypeface(txtTitle, getString(R.string.font_handwritting_bold));

		setTypeface(txt1, getString(R.string.font_handwritting_bold));
		setTypeface(txt2, getString(R.string.font_handwritting_bold));
		setTypeface(txt3, getString(R.string.font_handwritting_bold));
		setTypeface(txt4, getString(R.string.font_handwritting_bold));
		setTypeface(txt5, getString(R.string.font_handwritting_bold));
		setTypeface(txt6, getString(R.string.font_handwritting_bold));
		setTypeface(txt7, getString(R.string.font_handwritting_bold));
		setTypeface(txt8, getString(R.string.font_handwritting_bold));
		setTypeface(txt9, getString(R.string.font_handwritting_bold));
		setTypeface(txt10, getString(R.string.font_handwritting_bold));
		setTypeface(txt11, getString(R.string.font_handwritting_bold));
		setTypeface(txt12, getString(R.string.font_handwritting_bold));
		setTypeface(txt13, getString(R.string.font_handwritting_bold));
		setTypeface(txt14, getString(R.string.font_handwritting_bold));
		setTypeface(txt15, getString(R.string.font_handwritting_bold));
		setTypeface(txt16, getString(R.string.font_handwritting_bold));
		setTypeface(txt17, getString(R.string.font_handwritting_bold));
		setTypeface(txt18, getString(R.string.font_handwritting_bold));
		setTypeface(txt19, getString(R.string.font_handwritting_bold));
		setTypeface(txt20, getString(R.string.font_handwritting_bold));
		setTypeface(txt21, getString(R.string.font_handwritting_bold));
		setTypeface(txt22, getString(R.string.font_handwritting_bold));
		setTypeface(txt23, getString(R.string.font_handwritting_bold));
		setTypeface(txt24, getString(R.string.font_handwritting_bold));

		setTypeface(txtRealName, getString(R.string.font_handwritting_bold));
		setTypeface(txtFrndCallMe, getString(R.string.font_handwritting_bold));
		setTypeface(txtBirthDate, getString(R.string.font_handwritting_bold));
		setTypeface(txtEmail, getString(R.string.font_handwritting_bold));
		setTypeface(txtBestFrnd, getString(R.string.font_handwritting_bold));
		setTypeface(txtHappiestMoment,
				getString(R.string.font_handwritting_bold));
		setTypeface(txtEmbarrassingMoment,
				getString(R.string.font_handwritting_bold));
		setTypeface(txtThreePlace, getString(R.string.font_handwritting_bold));
		setTypeface(txtFavColor, getString(R.string.font_handwritting_bold));
		setTypeface(txtFavSong, getString(R.string.font_handwritting_bold));
		setTypeface(txtFavMovie, getString(R.string.font_handwritting_bold));
		setTypeface(txtLuckyNo, getString(R.string.font_handwritting_bold));
		setTypeface(txtGoodThing, getString(R.string.font_handwritting_bold));
		setTypeface(txtBadThing, getString(R.string.font_handwritting_bold));
		setTypeface(txtFunniestMoment,
				getString(R.string.font_handwritting_bold));
		setTypeface(txtThreeWish, getString(R.string.font_handwritting_bold));
		setTypeface(txtIfYouCanInvisible,
				getString(R.string.font_handwritting_bold));
		setTypeface(txtIfYouGetSuperpower,
				getString(R.string.font_handwritting_bold));
		setTypeface(txtBiggestWhatIf,
				getString(R.string.font_handwritting_bold));
		setTypeface(txtLifeIsHellWithout,
				getString(R.string.font_handwritting_bold));
		setTypeface(txtWakingOnMonday,
				getString(R.string.font_handwritting_bold));
		setTypeface(txtWorstMistake, getString(R.string.font_handwritting_bold));
		setTypeface(txtCommentAboutMe,
				getString(R.string.font_handwritting_bold));
		setTypeface(txtFavQuote, getString(R.string.font_handwritting_bold));
	}

	private void setLisener() {
		imgBtnBack.setOnClickListener(this);
		imgBtnDelete.setOnClickListener(this);

		imgBtnBack.setOnTouchListener(this);
		imgBtnDelete.setOnTouchListener(this);
	}

	@Override
	public int getLayoutId() {
		// TODO Auto-generated method stub
		return R.layout.layout_view_slam_detail;
	}

	private void setSlamDetails() {
		HashMap<String, String> slamData = MySlamBookDAO.getSlamBookDetailsBySlamId(getIntent()
				.getStringExtra("slamId"));

		if(!slamData.get("photo").equals("")){
			imgDp.setImageBitmap(convertBase64ToBitmap(slamData.get("photo")));
		}
		txtRealName.setText(slamData.get("realName"));
		txtFrndCallMe.setText(slamData.get("nickName"));
		txtBirthDate.setText(slamData.get("birthdate"));
		txtEmail.setText(slamData.get("emailId"));
		txtBestFrnd.setText(slamData.get("bestFriend"));
		txtHappiestMoment.setText(slamData.get("happiestMomentOfLife"));
		txtEmbarrassingMoment.setText(slamData.get("embarrassingMomentOfLife"));
		txtThreePlace.setText(slamData.get("threePlaceToSee"));
		txtFavColor.setText(slamData.get("favColor"));
		txtFavSong.setText(slamData.get("favSong"));
		txtFavMovie.setText(slamData.get("favMovie"));
		txtLuckyNo.setText(slamData.get("luckyNo"));
		txtGoodThing.setText(slamData.get("goodThing"));
		txtBadThing.setText(slamData.get("badThing"));
		txtFunniestMoment.setText(slamData.get("funniestThingHappned"));
		txtThreeWish.setText(slamData.get("threeWish"));
		txtIfYouCanInvisible.setText(slamData.get("ifYouCanInvisible"));
		txtIfYouGetSuperpower.setText(slamData.get("ifYouGetSuperPower"));
		txtBiggestWhatIf.setText(slamData.get("biggestWhatIf"));
		txtLifeIsHellWithout.setText(slamData.get("lifeIsHellWithout"));
		txtWakingOnMonday.setText(slamData.get("wakingUpMonday"));
		txtWorstMistake.setText(slamData.get("worstMistake"));
		txtCommentAboutMe.setText(slamData.get("commentAboutMe"));
		txtFavQuote.setText(slamData.get("favQuote"));
	}

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		switch (view.getId()) {
		case R.id.imgBtnBack:
			onBackPressed();
			break;

		case R.id.imgBtnDelete:
			deleteSlam();
			break;
		}
	}

	private void deleteSlam() {
		
		AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
		builder.setMessage(getString(R.string.delete_message))
		.setTitle(getString(R.string.alert))
		.setCancelable(false)
		.setPositiveButton(getString(R.string.yes),
				new DialogInterface.OnClickListener() {				
			@Override
			public void onClick(DialogInterface arg0, int arg1) {

				MySlamBookDAO.deleteSlamById(mContext, getIntent()
						.getStringExtra("slamId"));
				intent = new Intent(mContext, ViewSlamListActivity.class);
				startActivity(intent);
				finish();
			}
		})

		.setNegativeButton(getString(R.string.no),
				new DialogInterface.OnClickListener() {				
			@Override
			public void onClick(DialogInterface arg0, int arg1) {

			}
		});

		AlertDialog alert = builder.create();
		alert.show();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		intent = new Intent(mContext, ViewSlamListActivity.class);
		startActivity(intent);
		finish();
	}
}
