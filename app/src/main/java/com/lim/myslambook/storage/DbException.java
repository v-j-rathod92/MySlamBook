package com.lim.myslambook.storage;

@SuppressWarnings("serial")
public class DbException extends Exception {

	public DbException(String message) {
		super(message);
	}
}
