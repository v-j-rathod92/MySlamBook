package com.lim.myslambook.picker;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

@SuppressLint("ValidFragment")
public class DatePickerFragment extends DialogFragment {
	 OnDateSetListener ondateSet;
	 DatePickerDialog dialog;
	 Activity act;

	 @SuppressLint("ValidFragment")
	 public DatePickerFragment(Activity activity) {
		 act = activity;
	 }

	 public void setCallBack(OnDateSetListener ondate) {
	  ondateSet = ondate;
	 }

	 private int year, month, day;

	 @Override
	 public void setArguments(Bundle args) {
	  super.setArguments(args);
	  year = args.getInt("year");
	  month = args.getInt("month");
	  day = args.getInt("day");
	 }

	 @Override
	 public Dialog onCreateDialog(Bundle savedInstanceState) {		 
		 dialog = new DatePickerDialog(act, ondateSet, year, month, day);
		 return dialog;
	 }
	}  