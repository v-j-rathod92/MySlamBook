package com.lim.myslambook.splash;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.lim.myslambook.R;
import com.lim.myslambook.slam.DashBoardActivity;

public class SplashActivity extends Activity {

    private Thread _splash;
    private Intent intent;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_splash);

        mContext = this;
        _splash = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(3000);
                    intent = new Intent(mContext, DashBoardActivity.class);
                    startActivity(intent);
                    finish();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        _splash.start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        _splash.interrupt();
    }
}
