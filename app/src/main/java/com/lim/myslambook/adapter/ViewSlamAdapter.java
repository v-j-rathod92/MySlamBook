package com.lim.myslambook.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

import com.lim.myslambook.R;
import com.lim.myslambook.slam.ViewSlamDetailActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

public class ViewSlamAdapter extends BaseAdapter implements OnTouchListener {

	private ArrayList<HashMap<String, String>> slamList;
	private ArrayList<HashMap<String, String>> worldpopulationlist = null;
	private Context mContext;
	private LayoutInflater inflater;
	private Intent intent;

	public ViewSlamAdapter(ArrayList<HashMap<String, String>> slamList,
			Context context) {
		this.slamList = slamList;
		this.mContext = context;
		worldpopulationlist = new ArrayList<HashMap<String, String>>();
		worldpopulationlist.addAll(slamList);
		inflater = LayoutInflater.from(mContext);
	}

	@Override
	public int getCount() {
		return slamList.size();
	}

	@Override
	public Object getItem(int pos) {
		return slamList.get(pos);
	}

	@Override
	public long getItemId(int id) {
		return 0;
	}

	@Override
	public View getView(final int pos, View view, ViewGroup viewGroup) {
		ViewHolder holder = new ViewHolder();

		if (view == null) {
			view = inflater.inflate(R.layout.layout_adapter_view_slam, null,
					false);
			holder.btnSelectSlam = (Button) view
					.findViewById(R.id.btnSlamSelect);

			holder.btnSelectSlam.setTypeface(Typeface.createFromAsset(
					mContext.getAssets(),
					mContext.getString(R.string.font_handwritting_bold)));

			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}

		holder.btnSelectSlam.setText(slamList.get(pos).get("realName"));
		holder.btnSelectSlam.setOnTouchListener(this);
		
		holder.btnSelectSlam.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				intent = new Intent(mContext, ViewSlamDetailActivity.class);
				intent.putExtra("slamId", slamList.get(pos).get("slamId"));
				mContext.startActivity(intent);
				((Activity) mContext).finish();
			}
		});

//		holder.btnSelectSlam.setOnLongClickListener(new View.OnLongClickListener() {
//			@Override
//			public boolean onLongClick(View view) {
//				displayOptionDialog();
//				return false;
//			}
//		});
		return view;
	}

	static class ViewHolder {
		Button btnSelectSlam;
	}

	@Override
	public boolean onTouch(View view, MotionEvent motionEvent) {
		// TODO Auto-generated method stub
		if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
			view.setAlpha(0.7f);
		} else {
			view.setAlpha(1f);
		}

		return false;
	}

//	private void displayOptionDialog(){
//		TextView txtDialogTitle;
//		ImageButton imgBtnShareFb,imgBtnDelete;
//
//		Dialog dialog = new Dialog(mContext);
//		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//		dialog.setContentView(R.layout.layout_dialog);
//
//
//		txtDialogTitle = (TextView) dialog.findViewById(R.id.txtDialogOption);
//		txtDialogTitle.setTypeface(Typeface.createFromAsset(
//				mContext.getAssets(),
//				mContext.getString(R.string.font_handwritting_bold)));
//
//		imgBtnShareFb = (ImageButton) dialog.findViewById(R.id.imgBtnShareFb);
//		imgBtnDelete = (ImageButton) dialog.findViewById(R.id.imgBtnDelete);
//
//		imgBtnShareFb.setOnTouchListener(this);
//		imgBtnDelete.setOnTouchListener(this);
//		dialog.show();
//
//		Bundle params = new Bundle();
//		params.putString("name", "Facebook SDK for Android");
//		params.putString("caption", "Build great social apps and get more installs.");
//		params.putString("description", "The Facebook SDK for Android makes it easier and faster to develop Facebook integrated Android apps.");
//		params.putString("link", "https://developers.facebook.com/android");
//		params.putString("picture", "https://raw.github.com/fbsamples/ios-3.x-howtos/master/Images/iossdk_logo.png");
//
//
//	}


	public void filter(String charText) {
		charText = charText.toLowerCase(Locale.getDefault());
		slamList.clear();
		if (charText.length() == 0) {
			slamList.addAll(worldpopulationlist);
		}
		else
		{
			for (HashMap<String, String> wp : worldpopulationlist)
			{
				if (wp.get("realName").toLowerCase(Locale.getDefault()).contains(charText))
				{
					slamList.add(wp);
				}
			}
		}
		notifyDataSetChanged();
	}
}
